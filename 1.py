def dec_counter(func):
    def wrapper(a):
        wrapper.count+=1
        return func(a)
    wrapper.count = 0
    return wrapper

@dec_counter
def factorial(a):
    return 1 if a==1 else a*factorial(a-1)

print(factorial(5))
print(factorial.count)

