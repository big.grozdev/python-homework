from time import *
def dec_time(func):
    def wrapper(a,b):
        t = time()
        result = func(a,b)
        t = time() - t
        print(t)
        return result
    return wrapper

@dec_time
def plus(a,b):
    return a+b

print(plus(1,2))
