def date(day,month,year):
    if month in [1,3,5,7,8,10,12] and day in range(1,32):
        return True
    if month in [4,6,9,11,] and day in range(1,31):
        return True
    if month == 2:
        if day in range(1,29):
            return True
        if day == 29:
            if year % 4 == 0:
                if year % 100 != 0:
                    return True
                elif year % 400 == 0:
                    return True
                return False
    return False
print(date(29,2,1900))
