def merge_dict(a,b):
    return {**a,**b}
my_dict = {"1":1,"2":3}
your_dict = {"3":2,"4":4}
our_dict = merge_dict(my_dict,your_dict)
print(our_dict)
